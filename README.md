# AutoDispose
AutoDispose is an RxJava tool library for automatically binding the execution of RxJava streams to provided scope via disposal/cancellation.
Its helps to auto dispose an Observable based on event to avoid memory-leak. 


# AutoDispose includes :
* Bind with the openharmony OS Event
* Support for the Ability Lifecycle event.
* Support for the AbilitySlice Lifecycle event
* Support for the Fraction Lifecycle event.
* Support for the FractionAbility Lifecycle event.


# Usage Instructions
      
1. Make the Ability/Fraction/FractionAbility/AbilitySlice class which will implement the autodispose features.

Add this code into onStart method
 ```
		  Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_1, "onStart -> DISPOSING subscription in onStop()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_1, "onStart -> STARTED in onStart(), running until "
                                    + "onStop(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_1, "Error in subscribe in onStart "
                                    + throwable.getMessage());
                        }
                );
  ```
 Check the log to validate output
 # Installation tutorial
 
   AutoDispose is dependent on rxjava3
        
   1.  For using Autodispose modules in your sample application, add below dependencies to generate hap/har:
       
Modify entry build.gradle as below :
```
  dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
     implementation 'io.reactivex.rxjava3:rxjava:3.0.4'
     compile project(path: ':autodispose')
     compile project(path: ':autodispose_annotation')
     compile project(path: ':autodispose_harmony')
     compile project(path: ':autodispose_harmony_lifecycle')
     compile project(path: ':autodispose_lifecycle')
  }
  ```

2. For using Autodispose in separate application, add the below dependencies and include "autodispose.har", "autodispose_annotation.har", "autodispose_harmony.har" , "autodispose_harmony_lifecycle.har"  and "autodispose_lifecycle" in libs folder of "entry" module :


  Modify entry build.gradle as below :

 ```
 dependencies {
     implementation fileTree(dir: 'libs', include: ['*.har'])
     implementation 'io.reactivex.rxjava3:rxjava:3.0.4'
 }
 ```
 
 3.For using Autodispose from remote repository in separate application, add the below dependency in "entry" build.gradle.
Modify entry build.gradle as below :
```
  dependencies {
      implementation 'io.openharmony.tpc.thirdlib:autodispose:1.0.2'
      implementation 'io.openharmony.tpc.thirdlib:autodispose_annotation:1.0.2'
      implementation 'io.openharmony.tpc.thirdlib:autodispose_harmony:1.0.2'
      implementation 'io.openharmony.tpc.thirdlib:autodispose_harmony_lifecycle:1.0.2'
      implementation 'io.openharmony.tpc.thirdlib:autodispose_lifecycle:1.0.2'
  }
  ```

# License
   Copyright (C) 2017 Uber Technologies
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
