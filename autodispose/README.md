**AutoDispose**

AutoDispose is an RxJava tool library for automatically binding the execution of RxJava streams to provided scope via disposal/cancellation.

**Software architecture:**   		
		
        IDE version: 2.1 Beta2
		SDK version: 2.1.0.141

**Installation tutorial**
**Solution: local har package integration**

Method1

1. Add following har in entry libs folder:
        
        autodispose.har
2. Add following har in entry libs folder:
        
        autodispose_lifecycle.har
 3. Add following har in entry libs folder:
        
        autodispose_harmony_lifecycle.har
4. Add following jar in libaray libs folder:

		autodispose_harmony.jar
5. Add the following code to the gradle of the entry:
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
		implementation files('libs/autodispose.har')
        implementation files('libs/autodispose_harmony.har')
        implementation files('libs/autodispose_harmony_lifecycle.har')
        implementation files('libs/autodispose_lifecycle.har')
        implementation 'io.reactivex.rxjava3:rxjava:3.0.4'



Method2

allprojects {
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:autodispose:1.0.3'
implementation 'io.openharmony.tpc.thirdlib:autodispose_harmony:1.0.2'
implementation 'io.openharmony.tpc.thirdlib:autodispose_harmony_lifecycle:1.0.2'
implementation 'io.openharmony.tpc.thirdlib:autodispose_lifecycle:1.0.3'
implementation 'io.reactivex.rxjava3:rxjava:3.0.4'


**Usage Instructions:**
1. Add autodispose.har,autodispose_harmony.har,autodispose_lifecycle,autodispose_harmony_lifecycle.har file in the entry libs folder & add the same in build.gradle file:
		
        implementation files('libs/autodispose.har')
         implementation files('libs/autodispose_harmony.har')/
          implementation files('libs/autodispose_lifecycle.har')/
           implementation files('libs/autodispose_harmony_lifecycle.har')
        
2. Add rxjava dependency in entry build.gradle file:

		implementation 'io.reactivex.rxjava3:rxjava:3.0.4'

        
3. Make the Ability/Fraction/FractionAbility/AbilitySlice class which will implement the autodispose features.


**Step 1.** Add the dependency
```gradle
添加库的依赖
方式一：
添加har包到lib文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects {
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:autodispose:1.0.3'
implementation 'io.openharmony.tpc.thirdlib:autodispose_harmony:1.0.2'
implementation 'io.openharmony.tpc.thirdlib:autodispose_harmony_lifecycle:1.0.2'
implementation 'io.openharmony.tpc.thirdlib:autodispose_lifecycle:1.0.3'
```

Add this code into onStart method
 
		  Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_1, "onStart -> DISPOSING subscription in onStop()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_1, "onStart -> STARTED in onStart(), running until "
                                    + "onStop(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_1, "Error in subscribe in onStart "
                                    + throwable.getMessage());
                        }
                );
 Check the log to validate output


## License

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.