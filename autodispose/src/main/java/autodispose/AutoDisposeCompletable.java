/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.CompletableSource;

/**
 * Completable class.
 */
final class AutoDisposeCompletable extends Completable implements CompletableSubscribeProxy {
    private final Completable source;
    private final CompletableSource scope;

    AutoDisposeCompletable(Completable source, CompletableSource scope) {
        this.source = source;
        this.scope = scope;
    }

    @Override
    protected void subscribeActual(CompletableObserver completableObserver) {
        source.subscribe(new AutoDisposingCompletableObserverImpl(scope, completableObserver));
    }
}
