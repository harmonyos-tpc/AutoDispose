/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.*;
import io.reactivex.rxjava3.parallel.ParallelFlowableConverter;

/**
 * A custom converter that implements all the RxJava types converters, for use with the {@code as()}
 * operator.
 *
 * @param <T> the type.
 */
public interface AutoDisposeConverter<T>
        extends FlowableConverter<T, FlowableSubscribeProxy<T>>,
        ParallelFlowableConverter<T, ParallelFlowableSubscribeProxy<T>>,
        ObservableConverter<T, ObservableSubscribeProxy<T>>,
        MaybeConverter<T, MaybeSubscribeProxy<T>>,
        SingleConverter<T, SingleSubscribeProxy<T>>,
        CompletableConverter<CompletableSubscribeProxy> {
}
