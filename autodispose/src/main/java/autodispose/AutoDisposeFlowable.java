/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Flowable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

/**
 * AutoDisposeFlowable class.
 *
 * @param <T>
 */
final class AutoDisposeFlowable<T> extends Flowable<T> implements FlowableSubscribeProxy<T> {
    private final Publisher<T> source;
    private final CompletableSource scope;

    AutoDisposeFlowable(Publisher<T> source, CompletableSource scope) {
        this.source = source;
        this.scope = scope;
    }

    @Override
    protected void subscribeActual(Subscriber<? super T> observer) {
        source.subscribe(new AutoDisposingSubscriberImpl<>(scope, observer));
    }
}
