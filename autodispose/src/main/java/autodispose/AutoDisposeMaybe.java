/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.core.MaybeSource;

/**
 * class represents a deferred computation and emission of a single value.
 *
 * @param <T>
 */
final class AutoDisposeMaybe<T> extends Maybe<T> implements MaybeSubscribeProxy<T> {
    private final MaybeSource<T> source;
    private final CompletableSource scope;

    AutoDisposeMaybe(MaybeSource<T> source, CompletableSource scope) {
        this.source = source;
        this.scope = scope;
    }

    @Override
    protected void subscribeActual(MaybeObserver<? super T> observer) {
        source.subscribe(new AutoDisposingMaybeObserverImpl<>(scope, observer));
    }
}
