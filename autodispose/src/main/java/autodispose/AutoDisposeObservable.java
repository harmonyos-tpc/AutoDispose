/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Observer;

/**
 * Observable class.
 *
 * @param <T>
 */
final class AutoDisposeObservable<T> extends Observable<T> implements ObservableSubscribeProxy<T> {
    private final ObservableSource<T> source;
    private final CompletableSource scope;

    AutoDisposeObservable(ObservableSource<T> source, CompletableSource scope) {
        this.source = source;
        this.scope = scope;
    }

    @Override
    protected void subscribeActual(Observer<? super T> observer) {
        source.subscribe(new AutoDisposingObserverImpl<>(scope, observer));
    }
}
