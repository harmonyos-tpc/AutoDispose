/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.core.SingleSource;

/**
 * Single subscribe disposal.
 *
 * @param <T>
 */
final class AutoDisposeSingle<T> extends Single<T> implements SingleSubscribeProxy<T> {
    private final SingleSource<T> source;
    private final CompletableSource scope;

    AutoDisposeSingle(SingleSource<T> source, CompletableSource scope) {
        this.source = source;
        this.scope = scope;
    }

    @Override
    protected void subscribeActual(SingleObserver<? super T> observer) {
        source.subscribe(new AutoDisposingSingleObserverImpl<>(scope, observer));
    }
}
