/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.annotations.Nullable;

/**
 * Util class.
 */
final class AutoDisposeUtil {
    private AutoDisposeUtil() {
        throw new InstantiationError();
    }

    static <T> T checkNotNull(@Nullable T value, String message) {
        if (value == null) {
            throw new NullPointerException(message);
        } else {
            return value;
        }
    }
}
