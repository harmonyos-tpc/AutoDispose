/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import autodispose.observers.AutoDisposingMaybeObserver;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.DisposableCompletableObserver;

import java.util.concurrent.atomic.AtomicReference;

/**
 * AutoDisposingMaybeObserverImpl class.
 *
 * @param <T>
 */
final class AutoDisposingMaybeObserverImpl<T> implements AutoDisposingMaybeObserver<T> {
    @SuppressWarnings("WeakerAccess") // Package private for synthetic accessor saving
    final AtomicReference<Disposable> mainDisposable = new AtomicReference<>();

    @SuppressWarnings("WeakerAccess") // Package private for synthetic accessor saving
    final AtomicReference<Disposable> scopeDisposable = new AtomicReference<>();

    private final CompletableSource scope;
    private final MaybeObserver<? super T> delegate;

    AutoDisposingMaybeObserverImpl(CompletableSource scope, MaybeObserver<? super T> delegate) {
        this.scope = scope;
        this.delegate = delegate;
    }

    @Override
    public MaybeObserver<? super T> delegateObserver() {
        return delegate;
    }

    @Override
    public void onSubscribe(final Disposable d) {
        DisposableCompletableObserver o =
                new DisposableCompletableObserver() {
                    @Override
                    public void onError(Throwable e) {
                        scopeDisposable.lazySet(AutoDisposableHelper.DISPOSED);
                        AutoDisposingMaybeObserverImpl.this.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        scopeDisposable.lazySet(AutoDisposableHelper.DISPOSED);
                        AutoDisposableHelper.dispose(mainDisposable);
                    }
                };
        if (AutoDisposeEndConsumerHelper.setOnce(scopeDisposable, o, getClass())) {
            delegate.onSubscribe(this);
            scope.subscribe(o);
            AutoDisposeEndConsumerHelper.setOnce(mainDisposable, d, getClass());
        }
    }

    @Override
    public boolean isDisposed() {
        return mainDisposable.get() == AutoDisposableHelper.DISPOSED;
    }

    @Override
    public void dispose() {
        AutoDisposableHelper.dispose(scopeDisposable);
        AutoDisposableHelper.dispose(mainDisposable);
    }

    @Override
    public void onSuccess(T value) {
        if (!isDisposed()) {
            mainDisposable.lazySet(AutoDisposableHelper.DISPOSED);
            AutoDisposableHelper.dispose(scopeDisposable);
            delegate.onSuccess(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if (!isDisposed()) {
            mainDisposable.lazySet(AutoDisposableHelper.DISPOSED);
            AutoDisposableHelper.dispose(scopeDisposable);
            delegate.onError(e);
        }
    }

    @Override
    public void onComplete() {
        if (!isDisposed()) {
            mainDisposable.lazySet(AutoDisposableHelper.DISPOSED);
            AutoDisposableHelper.dispose(scopeDisposable);
            delegate.onComplete();
        }
    }
}
