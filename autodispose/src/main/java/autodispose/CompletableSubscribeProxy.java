/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.observers.TestObserver;

/**
 * Subscribe proxy that matches {@link Completable}'s subscribe overloads.
 */
public interface CompletableSubscribeProxy {
    /**
     * Proxy for {@link Completable#subscribe()}.
     *
     * @return a {@link Disposable}
     */
    Disposable subscribe();

    /**
     * Proxy for {@link Completable#subscribe(Action)}.
     *
     * @param action .
     * @return a {@link Disposable}
     */
    Disposable subscribe(Action action);

    /**
     * Proxy for {@link Completable#subscribe(Action, Consumer)}.
     *
     * @param onError .
     * @param action  .
     * @return a {@link Disposable}
     */
    Disposable subscribe(Action action, Consumer<? super Throwable> onError);

    /**
     * Proxy for {@link Completable#subscribe(CompletableObserver)}.
     *
     * @param observer .
     */
    void subscribe(CompletableObserver observer);

    /**
     * Proxy for {@link Completable#subscribeWith(CompletableObserver)}.
     *
     * @param observer value of observer.
     * @return <E> {@link CompletableObserver}
     */
    @CheckReturnValue
    <E extends CompletableObserver> E subscribeWith(E observer);

    /**
     * Proxy for {@link Completable#test()}.
     *
     * @return a {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<Void> test();

    /**
     * Proxy for {@link Completable#test(boolean)}.
     *
     * @param dispose boolean value to dispose observer or not.
     * @return a {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<Void> test(boolean dispose);
}
