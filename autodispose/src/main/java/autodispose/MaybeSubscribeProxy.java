/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */
package autodispose;

import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.observers.TestObserver;

/**
 * Subscribe proxy that matches {@link Maybe}'s subscribe overloads.
 *
 * @param <T> type of subscriber.
 */
public interface MaybeSubscribeProxy<T> {
    /**
     * Proxy for {@link Maybe#subscribe()}.
     *
     * @return a {@link Disposable}
     */
    Disposable subscribe();

    /**
     * Proxy for {@link Maybe#subscribe(Consumer)}.
     *
     * @param onSuccess requires consumer value.
     * @return a {@link Disposable}
     */
    Disposable subscribe(Consumer<? super T> onSuccess);

    /**
     * Proxy for {@link Maybe#subscribe(Consumer, Consumer)}.
     *
     * @param onSuccess value on success.
     * @param onError   value on error.
     * @return a {@link Disposable}
     */
    Disposable subscribe(Consumer<? super T> onSuccess, Consumer<? super Throwable> onError);

    /**
     * Proxy for {@link Maybe#subscribe(Consumer, Consumer, Action)}.
     *
     * @param onComplete completion value.
     * @param onError    in case of error.
     * @param onSuccess  value on success.
     * @return a {@link Disposable}
     */
    Disposable subscribe(
            Consumer<? super T> onSuccess, Consumer<? super Throwable> onError, Action onComplete);

    /**
     * Proxy for {@link Maybe#subscribe(MaybeObserver)}.
     *
     * @param observer kind of observer.
     */
    void subscribe(MaybeObserver<? super T> observer);

    /**
     * Proxy for {@link Maybe#subscribeWith(MaybeObserver)}.
     *
     * @param observer E value to which to subscribe with.
     * @param <E>      type of observer.
     * @return a {@link MaybeObserver}
     */
    @CheckReturnValue
    <E extends MaybeObserver<? super T>> E subscribeWith(E observer);

    /**
     * Proxy for {@link Maybe#test()}.
     *
     * @return a {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<T> test();

    /**
     * Proxy for {@link Maybe#test(boolean)}.
     *
     * @param dispose true/false value.
     * @return a {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<T> test(boolean dispose);
}
