/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.observers.TestObserver;

/**
 * Subscribe proxy that matches {@link Observable}'s subscribe overloads.
 *
 * @param <T>
 */
public interface ObservableSubscribeProxy<T> {
    /**
     * Proxy for {@link Observable#subscribe()}.
     *
     * @return {@link Disposable}
     */
    Disposable subscribe();

    /**
     * Proxy for {@link Observable#subscribe(Consumer)}.
     *
     * @param onNext .
     * @return {@link Disposable}
     */
    Disposable subscribe(Consumer<? super T> onNext);

    /**
     * Proxy for {@link Observable#subscribe(Consumer, Consumer)}.
     *
     * @param onNext  .
     * @param onError .
     * @return {@link Disposable}
     */
    Disposable subscribe(Consumer<? super T> onNext, Consumer<? super Throwable> onError);

    /**
     * Proxy for {@link Observable#subscribe(Consumer, Consumer, Action)}.
     *
     * @param onNext     .
     * @param onError    .
     * @param onComplete .
     * @return {@link Disposable}
     */
    Disposable subscribe(Consumer<? super T> onNext, Consumer<? super Throwable> onError, Action onComplete);

    /**
     * Proxy for {@link Observable#subscribe(Observer).
     *
     * @param observer .
     */
    void subscribe(Observer<? super T> observer);

    /**
     * Proxy for {@link Observable#subscribeWith(Observer)}.
     *
     * @param observer .
     * @return an {@link Observer}
     */
    @CheckReturnValue
    <E extends Observer<? super T>> E subscribeWith(E observer);

    /**
     * Proxy for {@link Observable#test()}.
     *
     * @return {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<T> test();

    /**
     * Proxy for {@link Observable#test(boolean)}.
     *
     * @param dispose .
     * @return {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<T> test(boolean dispose);
}
