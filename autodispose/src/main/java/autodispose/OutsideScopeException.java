/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

/**
 * Signifies an error occurred due to execution starting outside the lifecycle.
 *
 * @author rWX1019951
 */
public class OutsideScopeException extends RuntimeException {
    /**
     * Constructor.
     *
     * @param string .
     */
    public OutsideScopeException(String string) {
        super(string);
    }

    @Override
    public final synchronized Throwable fillInStackTrace() {
        if (AutoDisposePlugins.fillInOutsideScopeExceptionStacktraces) {
            return super.fillInStackTrace();
        } else {
            return this;
        }
    }
}
