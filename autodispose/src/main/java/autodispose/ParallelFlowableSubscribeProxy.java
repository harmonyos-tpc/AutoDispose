/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.parallel.ParallelFlowable;
import org.reactivestreams.Subscriber;

/**
 * Subscribe proxy that matches {@link ParallelFlowable}'s subscribe overloads.
 *
 * @author rWX1019951
 */
public interface ParallelFlowableSubscribeProxy<T> {
    /**
     * Proxy for {@link ParallelFlowable#subscribe(Subscriber[])}.
     *
     * @param subscribers array of elements that subscribed.
     */
    void subscribe(Subscriber<? super T>[] subscribers);
}
