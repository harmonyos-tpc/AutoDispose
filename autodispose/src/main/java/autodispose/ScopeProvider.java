/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import autodispose.internal.DoNotMock;
import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;

/**
 * Provides {@link CompletableSource} representation of scope. The emission of this is the
 * signal
 *
 * @author rWX101995
 */
@DoNotMock(value = "Use TestScopeProvider instead")
public interface ScopeProvider {
    /**
     * New provider that is "unbound", e.g. will emit completion event to signal that the scope is
     * unbound.
     */
    ScopeProvider UNBOUND = Completable::never;

    /**
     * Provide scope.
     *
     * @return {@link CompletableSource} that, upon completion, will trigger disposal.
     * @throws Exception scope retrievals throws an exception, such as {@link OutsideScopeException}
     */
    @CheckReturnValue
    CompletableSource requestScope() throws Exception;
}
