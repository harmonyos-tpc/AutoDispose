/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.BiConsumer;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.observers.TestObserver;

/**
 * Subscribe proxy that matches {@link Single}'s subscribe overloads.
 *
 * @author rWX1019951
 * @noinspection ALL
 */
public interface SingleSubscribeProxy<T> {
    /**
     * Proxy for {@link Single#subscribe()}.
     *
     * @return {@link Disposable}
     */
    Disposable subscribe();

    /**
     * Proxy for {@link Single#subscribe(Consumer)}.
     *
     * @param onSuccess
     * @return {@link Disposable}
     * @noinspection checkstyle:JavadocMethod
     */
    Disposable subscribe(Consumer<? super T> onSuccess);

    /**
     * Proxy for {@link Single#subscribe(BiConsumer)}.
     *
     * @param biConsumer
     * @return {@link Disposable}
     * @noinspection checkstyle:JavadocMethod
     */
    Disposable subscribe(BiConsumer<? super T, ? super Throwable> biConsumer);

    /**
     * Proxy for {@link Single#subscribe(Consumer, Consumer)}.
     *
     * @param onSuccess
     * @param onError
     * @return {@link Disposable}
     */
    Disposable subscribe(Consumer<? super T> onSuccess, Consumer<? super Throwable> onError);

    /**
     * Proxy for {@link Single#subscribe(SingleObserver)}.
     *
     * @param observer
     */
    void subscribe(SingleObserver<? super T> observer);

    /**
     * Proxy for {@link Single#subscribeWith(SingleObserver)}.
     *
     * @param observer
     * @return {@link SingleObserver}
     */
    @CheckReturnValue
    <E extends SingleObserver<? super T>> E subscribeWith(E observer);

    /**
     * Proxy for {@link Single#test()}.
     *
     * @return {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<T> test();

    /**
     * Proxy for {@link Single#test(boolean)}.
     *
     * @param dispose
     * @return {@link TestObserver}
     */
    @CheckReturnValue
    TestObserver<T> test(boolean dispose);
}
