/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.subjects.CompletableSubject;

/**
 * ScopeProvider implementation for testing. You can either back it with your own instance,
 * or just stub it in place and use its public emit APIs.
 *
 * @author rWX1019951
 */
public final class TestScopeProvider implements ScopeProvider {
    private final CompletableSubject innerScope = CompletableSubject.create();

    @SuppressWarnings("AutoDispose")
    private TestScopeProvider(Completable delegate) {
        delegate.subscribe(innerScope);
    }

    /**
     * Creates new provider backed by an internal {@link CompletableSubject}. Useful for stubbing or
     * if you only want to use the emit APIs
     *
     * @return the created TestScopeProvider.
     */
    public static TestScopeProvider create() {
        return create(CompletableSubject.create());
    }

    /**
     * Creates new provider backed by {@code delegate}.
     *
     * @param delegate the delegate to back this with.
     * @return the created TestScopeProvider.
     */
    public static TestScopeProvider create(Completable delegate) {
        return new TestScopeProvider(delegate);
    }

    /**
     * Method is created to provide the inner scope.
     *
     * @return inner scope
     */
    @Override
    public CompletableSource requestScope() {
        return innerScope;
    }

    /**
     * Emits success event, just simple Object.
     */
    public void emit() {
        innerScope.onComplete();
    }
}
