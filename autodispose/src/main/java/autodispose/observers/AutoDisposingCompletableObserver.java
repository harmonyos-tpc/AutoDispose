/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.observers;

import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * A {@link Disposable} {@link CompletableObserver} that can automatically dispose itself. Interface
 * here for type safety but enforcement is left to the implementation.
 */
public interface AutoDisposingCompletableObserver extends CompletableObserver, Disposable {
    /**
     * CompletableObserver method.
     *
     * @return The delegate {@link CompletableObserver} that is used under the hood for introspection
     * purposes.
     */
    CompletableObserver delegateObserver();
}
