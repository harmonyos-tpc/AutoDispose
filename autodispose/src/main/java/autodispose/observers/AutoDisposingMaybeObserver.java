/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */
package autodispose.observers;

import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * A {@link Disposable} {@link MaybeObserver} that can automatically dispose itself. Interface here
 * for type safety but enforcement is left to the implementation.
 *
 * @param <T> can take any type of MaybeObserver.
 */
public interface AutoDisposingMaybeObserver<T> extends MaybeObserver<T>, Disposable {
    /**
     * Delegate observer part.
     *
     * @return The delegate {@link MaybeObserver} that is used under the hood for introspection
     * purposes.
     */
    MaybeObserver<? super T> delegateObserver();
}
