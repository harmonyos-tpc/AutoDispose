/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.observers;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * A {@link Disposable} {@link Observer} that can automatically dispose itself. Interface here for
 * type safety but enforcement is left to the implementation.
 */
public interface AutoDisposingObserver<T> extends Observer<T>, Disposable {
    /**
     * delegateObserver method.
     *
     * @return The delegate {@link Observer} that is used under the hood for introspection purposes.
     */
    Observer<? super T> delegateObserver();
}
