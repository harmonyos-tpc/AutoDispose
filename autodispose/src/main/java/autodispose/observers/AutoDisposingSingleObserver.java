/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.observers;

import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * A {@link Disposable} {@link SingleObserver} that can automatically dispose itself. Interface here
 * for type safety but enforcement is left to the implementation.
 */
public interface AutoDisposingSingleObserver<T> extends SingleObserver<T>, Disposable {

    /**
     * delegateObserver method.
     *
     * @return The delegate {@link SingleObserver} that is used under the hood for introspection
     * purposes.
     */
    SingleObserver<? super T> delegateObserver();
}
