/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.observers;

import io.reactivex.rxjava3.core.FlowableSubscriber;
import io.reactivex.rxjava3.disposables.Disposable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

/**
 * A {@link Disposable} {@link Subscriber} that can automatically dispose itself. Interface here for
 * type safety but enforcement is left to the implementation.
 */
public interface AutoDisposingSubscriber<T> extends FlowableSubscriber<T>, Subscription, Disposable {

    /**
     * delegateObserver method.
     *
     * @return The delegate {@link Subscriber} that is used under the hood for introspection purposes.
     */
    Subscriber<? super T> delegateSubscriber();
}
