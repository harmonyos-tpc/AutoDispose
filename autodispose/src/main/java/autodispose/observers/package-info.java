/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

/**
 * These are Observers AutoDispose uses when scoping an observable. They are exposed as a public API
 * to allow for consumers to watch for them if they want, such as in RxJava plugins.
 */
package autodispose.observers;
