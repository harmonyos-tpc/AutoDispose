/*
 * Copyright (C) 2019. Uber Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.harmony;

import autodispose.harmony.schedulers.HarmonySchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import ohos.eventhandler.EventRunner;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Copy of the MainThreadDisposable from Rxohos which makes use of {@link
 * MainThreadDisposable#isMainThread()}. This allows disposing on the JVM without crashing due to
 * the looper check (which is often stubbed in tests).
 */
public abstract class MainThreadDisposable implements Disposable {
    public static boolean isMainThread() {
        return EventRunner.current() == EventRunner.getMainEventRunner();
    }

    private final AtomicBoolean unsubscribed = new AtomicBoolean();

    /**
     * Verify that the calling thread is the openharmony main thread.
     * <p>
     * Calls to this method are usually preconditions for subscription behavior which instances of
     * this class later undo. See the class documentation for an example.
     *
     * @return true/false whether its main thread or not.
     * @throws IllegalStateException when called from any other thread.
     */


    @Override
    public final boolean isDisposed() {
        return unsubscribed.get();
    }

    @Override
    public final void dispose() {
        if (unsubscribed.compareAndSet(false, true)) {
            if (EventRunner.current() != EventRunner.getMainEventRunner()) {
                onDispose();
            } else {
                HarmonySchedulers.mainThread().scheduleDirect(this::onDispose);
            }
        }
    }

    /**
     * onDispose abstract method.
     */
    protected abstract void onDispose();
}
