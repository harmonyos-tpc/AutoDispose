/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.harmony.schedulers;

import autodispose.harmony.util.LogUtil;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import ohos.eventhandler.EventHandler;

import java.util.concurrent.TimeUnit;

/**
 * Scheduler handler class
 */
final class HandlerScheduler extends Scheduler {
    private static final String TAG = "HandlerScheduler";
    private final EventHandler eventhandler;
    private final boolean isAsync;

    HandlerScheduler(EventHandler eventhandler, boolean async) {
        this.eventhandler = eventhandler;
        this.isAsync = async;
    }

    @Override
    // Async will only be true when the API is available to call.
    public Disposable scheduleDirect(Runnable run, long delay, TimeUnit unit) {
        if (run == null) {
            throw new NullPointerException("run == null");
        }
        if (unit == null) {
            throw new NullPointerException("unit == null");
        }

        run = RxJavaPlugins.onSchedule(run);
        ScheduledRunnable scheduled = new ScheduledRunnable(eventhandler, run);

        /* This will delay calling run after that many ms */
        eventhandler.postTask(scheduled, unit.toMillis(delay));
        return scheduled;
    }

    @Override
    public Worker createWorker() {
        return new HandlerWorker(eventhandler, isAsync);
    }

    /**
     * Event handler class that extends worker.
     */
    private static final class HandlerWorker extends Worker {
        private final EventHandler eventhandler;
        private final boolean async;

        private volatile boolean disposed;

        HandlerWorker(EventHandler eventhandler, boolean async) {
            this.eventhandler = eventhandler;
            this.async = async;
        }

        @Override
        // Async will only be true when the API is available to call.
        public Disposable schedule(Runnable run, long delay, TimeUnit unit) {
            if (run == null) {
                throw new NullPointerException("run == null");
            }
            if (unit == null) {
                throw new NullPointerException("unit == null");
            }

            LogUtil.info(TAG, "Entering schedule");

            if (disposed) {
                return Disposable.disposed();
            }

            run = RxJavaPlugins.onSchedule(run);

            ScheduledRunnable scheduled = new ScheduledRunnable(eventhandler, run);
            eventhandler.postTask(scheduled, unit.toMillis(delay));

            // Re-check disposed state for removing in case we were racing a call to dispose().
            if (disposed) {
                eventhandler.removeAllEvent();
                return Disposable.disposed();
            }

            return scheduled;
        }

        @Override
        public void dispose() {
            LogUtil.info(TAG, "Entering HandlerWorker dispose");
            disposed = true;
            eventhandler.removeAllEvent();
        }

        @Override
        public boolean isDisposed() {
            return disposed;
        }
    }

    /**
     * Runnable class for scheduler.
     */
    private static final class ScheduledRunnable implements Runnable, Disposable {
        private final EventHandler eventhandler;
        private final Runnable delegate;

        private volatile boolean disposed; // Tracked solely for isDisposed().

        ScheduledRunnable(EventHandler eventhandler, Runnable delegate) {
            this.eventhandler = eventhandler;
            this.delegate = delegate;
        }

        @Override
        public void run() {
            try {
                LogUtil.info(TAG, "Entering run");
                delegate.run();
            } catch (Throwable t) {
                RxJavaPlugins.onError(t);
            }
        }

        @Override
        public void dispose() {
            LogUtil.info(TAG, "Entering ScheduledRunnable dispose");
            eventhandler.removeAllEvent();
            disposed = true;
        }

        @Override
        public boolean isDisposed() {
            return disposed;
        }
    }
}
