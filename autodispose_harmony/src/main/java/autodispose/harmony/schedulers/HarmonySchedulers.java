/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.harmony.schedulers;

import autodispose.harmony.plugins.RxHarmonyPlugins;
import autodispose.harmony.util.LogUtil;
import io.reactivex.rxjava3.core.Scheduler;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * Harmony-specific Schedulers.
 */
public final class HarmonySchedulers {
    private static final String TAG = "HarmonySchedulers";

    /**
     * Returns scheduler for event.
     */
    private static final class MainHolder {
        static final Scheduler DEFAULT
                = new HandlerScheduler(new EventHandler(EventRunner.getMainEventRunner()), true);
    }

    private static final Scheduler MAIN_THREAD =
            RxHarmonyPlugins.initMainThreadScheduler(() -> MainHolder.DEFAULT);

    private HarmonySchedulers() {
        throw new AssertionError("No instances.");
    }

    /**
     * A {@link Scheduler} which executes actions on the Harmony main thread.
     * <p>
     * The returned scheduler will post asynchronous messages to the looper by default.
     *
     * @return gives scheduler.
     * @see #from(EventRunner, boolean)
     */
    public static Scheduler mainThread() {
        return RxHarmonyPlugins.onMainThreadScheduler(MAIN_THREAD);
    }

    /**
     * A {@link Scheduler} which executes actions on {@code EventRunner}.
     * <p>
     * The returned scheduler will post asynchronous messages to the EventRunner by default.
     *
     * @param eventRunner Pass the event
     * @return scheduler is returned.
     * @see #from(EventRunner, boolean)
     */
    public static Scheduler from(EventRunner eventRunner) {
        return from(eventRunner, true);
    }

    /**
     * A {@link Scheduler} which executes actions on {@code EventRunner}.
     *
     * @param async       if true, the scheduler will use async messaging
     * @param eventRunner Pass event
     *                    see Message#setAsynchronous(boolean)
     * @return scheduler is returned.
     */
    public static Scheduler from(EventRunner eventRunner, boolean async) {
        LogUtil.info(TAG, "Entering from");
        if (eventRunner == null) {
            throw new NullPointerException("eventRunner == null");
        }

        return new HandlerScheduler(new EventHandler(eventRunner) {
            @Override
            public void processEvent(InnerEvent event) {
            }
        }, async);
    }
}
