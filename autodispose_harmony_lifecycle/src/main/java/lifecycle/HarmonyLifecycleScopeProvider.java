/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lifecycle;

import autodispose.OutsideScopeException;
import autodispose.harmony.util.LogUtil;
import autodispose.lifecycle.CorrespondingEventsFunction;
import autodispose.lifecycle.LifecycleEndedException;
import autodispose.lifecycle.LifecycleScopeProvider;
import autodispose.lifecycle.LifecycleScopes;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Observable;
import ohos.aafwk.ability.ILifecycle;
import ohos.aafwk.ability.Lifecycle;

/**
 * A {@link LifecycleScopeProvider} that can provide scoping for Harmony {@link Lifecycle} and
 * {@link ILifecycle owner} classes.
 *
 * <p>
 *
 * <pre><code>
 * AutoDispose.autoDisposable(HarmonyLifecycleScopeProvider.from(lifecycleOwner))
 * </code></pre>
 *
 * @author Rohit rwx1000605
 */
public final class HarmonyLifecycleScopeProvider implements LifecycleScopeProvider<Lifecycle.Event> {
    private static final String TAG_LOG = HarmonyLifecycleScopeProvider.class.getName();

    private static final CorrespondingEventsFunction<Lifecycle.Event> DEFAULT_CORRESPONDING_EVENTS =
            lastEvent -> {
                switch (lastEvent) {
                    case UNDEFINED:
                        return Lifecycle.Event.ON_STOP;
                    case ON_START:
                        return Lifecycle.Event.ON_STOP;
                    case ON_ACTIVE:
                        return Lifecycle.Event.ON_INACTIVE;
                    case ON_FOREGROUND:
                        return Lifecycle.Event.ON_BACKGROUND;
                    case ON_STOP:
                        return Lifecycle.Event.ON_STOP;
                    case ON_BACKGROUND:
                        return Lifecycle.Event.ON_STOP;
                    default:
                        throw new LifecycleEndedException("Lifecycle has ended! Last event was " + lastEvent);
                }
            };

    private final LifecycleEventsObservable lifecycleObservable;
    private final CorrespondingEventsFunction<Lifecycle.Event> boundaryResolver;

    private HarmonyLifecycleScopeProvider(
            Lifecycle lifecycle, CorrespondingEventsFunction<Lifecycle.Event> boundaryResolver) {
        this.lifecycleObservable = new LifecycleEventsObservable(lifecycle);
        this.boundaryResolver = boundaryResolver;
    }

    /**
     * Creates {@link HarmonyLifecycleScopeProvider} for Harmony LifecycleOwners.
     *
     * @param owner the owner to scope for.
     * @return {@link HarmonyLifecycleScopeProvider} against this owner.
     */
    public static HarmonyLifecycleScopeProvider from(ILifecycle owner) {
        LogUtil.info(TAG_LOG, "--------- HarmonyLifecycleScopeProvider from(ILifecycle owner) -> "
                + "It'll return getLifecycle() = " + owner.getLifecycle().getLifecycleState());
        return from(owner.getLifecycle());
    }

    /**
     * Creates a {@link HarmonyLifecycleScopeProvider} for Harmony LifecycleOwners.
     *
     * @param owner      the owner to scope for.
     * @param untilEvent the event until the scope is valid.
     * @return a {@link HarmonyLifecycleScopeProvider} against this owner.
     */
    public static HarmonyLifecycleScopeProvider from(
            ILifecycle owner, Lifecycle.Event untilEvent) {
        return from(owner.getLifecycle(), untilEvent);
    }

    /**
     * Creates a {@link HarmonyLifecycleScopeProvider} for Harmony Lifecycles.
     *
     * @param lifecycle the lifecycle to scope for.
     * @return a {@link HarmonyLifecycleScopeProvider} against this lifecycle.
     */
    public static HarmonyLifecycleScopeProvider from(Lifecycle lifecycle) {
        LogUtil.info(TAG_LOG, "------------- HarmonyLifecycleScopeProvider from(Lifecycle lifecycle) -> It'll return "
                + "lifecycle for corresponding event/state " + lifecycle.getLifecycleState() + " is -> ");
        return from(lifecycle, DEFAULT_CORRESPONDING_EVENTS);
    }

    /**
     * Creates a {@link HarmonyLifecycleScopeProvider} for Harmony Lifecycles.
     *
     * @param lifecycle  the lifecycle to scope for.
     * @param untilEvent the event until the scope is valid.
     * @return a {@link HarmonyLifecycleScopeProvider} against this lifecycle.
     */
    public static HarmonyLifecycleScopeProvider from(
            Lifecycle lifecycle, Lifecycle.Event untilEvent) {
        return from(lifecycle, new UntilEventFunction(untilEvent));
    }

    /**
     * Creates a {@link HarmonyLifecycleScopeProvider} for Harmony Lifecycles.
     *
     * @param owner            the owner to scope for.
     * @param boundaryResolver function that resolves the event boundary.
     * @return a {@link HarmonyLifecycleScopeProvider} against this owner.
     */
    public static HarmonyLifecycleScopeProvider from(
            ILifecycle owner, CorrespondingEventsFunction<Lifecycle.Event> boundaryResolver) {
        return from(owner.getLifecycle(), boundaryResolver);
    }

    /**
     * Creates a {@link HarmonyLifecycleScopeProvider} for Harmony Lifecycles.
     *
     * @param lifecycle        the lifecycle to scope for.
     * @param boundaryResolver function that resolves the event boundary.
     * @return a {@link HarmonyLifecycleScopeProvider} against this lifecycle.
     */
    public static HarmonyLifecycleScopeProvider from(
            Lifecycle lifecycle, CorrespondingEventsFunction<Lifecycle.Event> boundaryResolver) {
        return new HarmonyLifecycleScopeProvider(lifecycle, boundaryResolver);
    }

    @Override
    public Observable<Lifecycle.Event> lifecycle() {
        return lifecycleObservable;
    }

    @Override
    public CorrespondingEventsFunction<Lifecycle.Event> correspondingEvents() {
        ;
        return boundaryResolver;
    }

    @Override
    public Lifecycle.Event peekLifecycle() {
        lifecycleObservable.backfillEvents();
        return lifecycleObservable.getValue();
    }

    @Override
    public CompletableSource requestScope() {
        return LifecycleScopes.resolveScopeFromLifecycle(this);
    }

    /**
     * Class that returns until event.
     */
    private static class UntilEventFunction implements CorrespondingEventsFunction<Lifecycle.Event> {
        private final Lifecycle.Event untilEvent;

        UntilEventFunction(Lifecycle.Event untilEvent) {
            this.untilEvent = untilEvent;
        }

        @Override
        public Lifecycle.Event apply(Lifecycle.Event event) throws OutsideScopeException {
            return untilEvent;
        }
    }
}
