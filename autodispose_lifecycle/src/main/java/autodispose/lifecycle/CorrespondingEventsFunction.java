/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.lifecycle;

import autodispose.OutsideScopeException;
import io.reactivex.rxjava3.functions.Function;

/**
 * A corresponding events function that acts as normal {@link Function} but ensures single event
 * type in the generic and tightens the possible exception thrown to {@link OutsideScopeException}.
 *
 * @param <E> the event type.
 */
public interface CorrespondingEventsFunction<E> extends Function<E, E> {
    /**
     * Given an event {@code event}, returns the next corresponding event that this lifecycle should
     * dispose on.
     *
     * @param event the source or start event.
     * @return the target event that should signal disposal.
     * @throws OutsideScopeException if the lifecycle exceeds its scope boundaries.
     */
    @Override
    E apply(E event) throws OutsideScopeException;
}
