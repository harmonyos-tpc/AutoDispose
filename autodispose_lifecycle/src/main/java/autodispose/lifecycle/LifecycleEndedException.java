/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.lifecycle;

import autodispose.OutsideScopeException;

/**
 * Signifies an error occurred due to execution starting after the lifecycle has ended.
 */
public class LifecycleEndedException extends OutsideScopeException {
    /**
     * LifecycleEndedException method.
     */
    public LifecycleEndedException() {
        this("Lifecycle has ended!");
    }

    /**
     * LifecycleEndedException method.
     *
     * @param str value of execption.
     */
    public LifecycleEndedException(String str) {
        super(str);
    }
}
