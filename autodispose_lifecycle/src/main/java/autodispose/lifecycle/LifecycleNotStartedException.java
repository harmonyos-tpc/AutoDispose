/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.lifecycle;

import autodispose.OutsideScopeException;

/**
 * Signifies an error occurred due to execution starting before the lifecycle has started.
 */
public class LifecycleNotStartedException extends OutsideScopeException {
    /**
     * LifecycleNotStartedException method.
     */
    public LifecycleNotStartedException() {
        this("Lifecycle hasn't started!");
    }

    /**
     * LifecycleNotStartedException method.
     *
     * @param value exception string is passed here.
     */
    public LifecycleNotStartedException(String value) {
        super(value);
    }
}
