/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package autodispose.lifecycle;

import autodispose.ScopeProvider;
import autodispose.internal.DoNotMock;
import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.annotations.Nullable;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Observable;

/**
 * A convenience interface that, when implemented, helps provide information to create {@link
 * ScopeProvider} implementations that resolve the next corresponding lifecycle event and construct
 * a {@link Completable} representation of it from the {@link #lifecycle()} stream.
 *
 * <p>Convenience resolver utilities for this can be found in {@link LifecycleScopes}.
 *
 * @param <E> the lifecycle event type.
 * @see LifecycleScopes
 */
@DoNotMock(value = "Use TestLifecycleScopeProvider instead")
public interface LifecycleScopeProvider<E> extends ScopeProvider {
    /**
     * Lifecycle observable.
     *
     * @return a sequence of lifecycle events. Note that completion of this lifecycle will also
     * trigger disposal
     */
    @CheckReturnValue
    Observable<E> lifecycle();

    /**
     * Corresponding event function.
     *
     * @return a sequence of lifecycle events. It's recommended to back this with a static instance to
     * avoid unnecessary object allocation.
     */
    @CheckReturnValue
    CorrespondingEventsFunction<E> correspondingEvents();

    /**
     * Last seen lifecycle event function.
     *
     * @return the last seen lifecycle event, or {@code null} if none. Note that is {@code null} is
     * returned at subscribe-time, it will be used as signal to throw {@link LifecycleNotStartedException}.
     */
    @Nullable
    E peekLifecycle();

    @Override
    default CompletableSource requestScope() {
        return LifecycleScopes.resolveScopeFromLifecycle(this);
    }
}
