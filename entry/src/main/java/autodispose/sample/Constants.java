/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.sample;

/**
 * Constants class
 *
 * @author Rohit rwx1000605
 */
public class Constants {
    /**
     * This is used for keeping track of ability or ability slice in next class
     */
    public static final String KEY_ABILITY = "ability";
    /**
     * This is used for keeping track of fraction or ability fraction in next class
     */
    public static final String KEY_FRACTION = "fraction";
}
