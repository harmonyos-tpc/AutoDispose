/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.sample;

import autodispose.sample.slice.TestAbilitySlice;
import io.reactivex.rxjava3.core.Observable;
import lifecycle.HarmonyLifecycleScopeProvider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import java.util.concurrent.TimeUnit;

import static autodispose.AutoDispose.autoDisposable;

/**
 * This is TestAbility class to test auto dispose in onStart,onActive and onForeground.
 *
 * @author Rohit rwx1000605
 */
public class TestAbility extends Ability {
    private static final String TAG_LOG = "TestAbility";
    private static final String TAG_1 = " : No 1 -> ";
    private static final String TAG_2 = " : No 2 -> ";
    private static final String TAG_3 = " : No 3 -> ";
    private static final long PERIOD = 1L;
    private boolean isDisplayAbilityLog = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG_LOG, "--------onStart()");
        isDisplayAbilityLog = intent.getBooleanParam(Constants.KEY_ABILITY, false);

        if (!isDisplayAbilityLog) {
            super.setMainRoute(TestAbilitySlice.class.getName());
            return;
        } else {
            setUIContent(ResourceTable.Layout_simple_fraction_layout);
        }

        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_1, "---------onStart -> "
                        + "DISPOSING subscription in onStop()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_1, "---------onStart -> "
                                    + "STARTED in onStart(), running until onStop(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_1, "Error in subscribe in onStart "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onActive() {
        super.onActive();
        LogUtil.info(TAG_LOG, "onActive()");
        if (!isDisplayAbilityLog) {
            return;
        }
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_2, "----------onActive -> DISPOSING subscription"
                        + " in onInActive()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_2, "----------onActive -> STARTED in onActive(), running "
                                    + "until onInActive(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_2, "----------Error in subscribe in onActive "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        if (!isDisplayAbilityLog) {
            return;
        }
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_3, "---------onForeground -> DISPOSING subscription"
                        + " in onBackground()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_3, "------------onForeground -> STARTED in onForeground(), running "
                                    + "until onBackground(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_3, "-------------Error in subscribe in onForeground "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        LogUtil.info(TAG_LOG, "onInactive()");
        if (!isDisplayAbilityLog) {
            return;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.info(TAG_LOG, "onStop()");
    }
}
