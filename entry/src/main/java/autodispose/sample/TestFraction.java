/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.sample;

import io.reactivex.rxjava3.core.Observable;
import lifecycle.HarmonyLifecycleScopeProvider;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.concurrent.TimeUnit;

import static autodispose.AutoDispose.autoDisposable;

/**
 * This is TestFraction class to test auto dispose in onStart,onActive and onForeground.
 *
 * @author rWX1000605
 */
public class TestFraction extends Fraction {
    private static final String TAG_LOG = TestFraction.class.getName();
    private static final long PERIOD = 1L;
    private static final String TAG_1 = " : No 4 -> ";
    private static final String TAG_2 = " : No 5 -> ";
    private static final String TAG_3 = " : No 6 -> ";
    private static final String TAG_4 = " : No 7 -> ";

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container,
                                            Intent intent) {
        final Component component = super.onComponentAttached(scatter, container, intent);
        LogUtil.info(TAG_LOG, "onComponentAttached");

        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_4, "---------onComponentAttached -> "
                        + "DISPOSING subscription in onComponentDetach()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_4, "---------onComponentAttached -> "
                                    + "STARTED in onComponentAttached(), running until onComponentDetach(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_4, "Error in subscribe in onComponentAttached "
                                    + throwable.getMessage());
                        }
                );
        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG_LOG, "onStart");

        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_1, "---------onStart -> "
                        + "DISPOSING subscription in onStop()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_1, "---------onStart -> "
                                    + "STARTED in onStart(), running until onStop(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_1, "Error in subscribe in onStart "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onActive() {
        super.onActive();
        LogUtil.info(TAG_LOG, "onActive");

        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_2, "---------onActive -> DISPOSING subscription"
                        + " in onInActive()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_2, "---------onActive -> STARTED in onActive(), running "
                                    + "until onInActive(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_2, "---------Error in subscribe in onActive "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);

        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_3, "---------onForeground -> DISPOSING subscription"
                        + " in onBackground()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_3, "---------onForeground -> STARTED in onForeground(), running "
                                    + "until onBackground(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_3, "---------Error in subscribe in onForeground "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        LogUtil.info(TAG_LOG, "onInactive");
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.info(TAG_LOG, "onStop");
    }

    @Override
    protected void onComponentDetach() {
        super.onComponentDetach();
        LogUtil.info(TAG_LOG, "onComponentDetach");
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        LogUtil.info(TAG_LOG, "onBackground");
    }
}
