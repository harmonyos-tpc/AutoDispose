/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.sample.slice;

import autodispose.sample.Constants;
import autodispose.sample.LogUtil;
import autodispose.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

/**
 * MainAbilitySlice class to launch different abilities.
 *
 * @author rWX1000605
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final String TAG_LOG = MainAbilitySlice.class.getName();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        LogUtil.info(TAG_LOG, "onStart()");
        (findComponentById(ResourceTable.Id_ability)).setClickedListener(this);
        (findComponentById(ResourceTable.Id_ability_slice)).setClickedListener(this);
        (findComponentById(ResourceTable.Id_fraction)).setClickedListener(this);
        (findComponentById(ResourceTable.Id_fraction_ability)).setClickedListener(this);
    }

    @Override
    public void onActive() {
        LogUtil.info(TAG_LOG, "onActive()");
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        LogUtil.info(TAG_LOG, "onForeground()");
        super.onForeground(intent);
    }

    private void launchTestAbility(boolean ability) {
        Intent intent2 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("autodispose.sample")
                .withAbilityName("autodispose.sample.TestAbility")
                .build();
        intent2.setOperation(operation);
        intent2.setParam(Constants.KEY_ABILITY, ability);
        startAbility(intent2);
    }

    private void launchTestFractionAbility(boolean fraction) {
        LogUtil.info(TAG_LOG, "launchTestFractionAbility");
        Intent intent2 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("autodispose.sample")
                .withAbilityName("autodispose.sample.TestFractionAbility")
                .build();
        intent2.setOperation(operation);
        intent2.setParam(Constants.KEY_FRACTION, fraction);
        startAbility(intent2);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_ability:
                launchTestAbility(true);
                break;
            case ResourceTable.Id_ability_slice:
                launchTestAbility(false);
                break;
            case ResourceTable.Id_fraction:
                launchTestFractionAbility(true);
                break;
            case ResourceTable.Id_fraction_ability:
                launchTestFractionAbility(false);
                break;
            default:
                break;
        }
    }
}
