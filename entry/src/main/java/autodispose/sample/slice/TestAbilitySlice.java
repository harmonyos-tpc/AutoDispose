/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package autodispose.sample.slice;

import autodispose.sample.LogUtil;
import autodispose.sample.ResourceTable;
import io.reactivex.rxjava3.core.Observable;
import lifecycle.HarmonyLifecycleScopeProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import java.util.concurrent.TimeUnit;

import static autodispose.AutoDispose.autoDisposable;

/**
 * This is Test Ability Slice class to test auto dispose in onStart,onActive and onForeground.
 *
 * @author rWX1000605
 */
public class TestAbilitySlice extends AbilitySlice {
    private static final String TAG_LOG = "TestAbilitySlice";
    private static final String TAG_1 = " : No 11 -> ";
    private static final String TAG_2 = " : No 12 -> ";
    private static final String TAG_3 = " : No 13 -> ";
    private static final long PERIOD = 1L;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_test);

        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_1, "onStart -> DISPOSING subscription in onStop()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_1, "onStart -> STARTED in onStart(), running until "
                                    + "onStop(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_1, "Error in subscribe in onStart "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_3, "onForeground -> DISPOSING subscription"
                        + " in onBackground()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_3, "onForeground -> STARTED in onForeground(), running "
                                    + "until onBackground(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_3, "Error in subscribe in onForeground "
                                    + throwable.getMessage());
                        }
                );
    }

    @Override
    public void onActive() {
        super.onActive();

        Observable.interval(1L, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG + TAG_2, "onActive -> DISPOSING subscription"
                        + " in onInActive()"))
                .to(autoDisposable(HarmonyLifecycleScopeProvider.from(this)))
                .subscribe(num -> {
                            LogUtil.info(TAG_LOG + TAG_2, "onActive -> STARTED in onActive(), running "
                                    + "until onInActive(): " + num);
                        }, throwable -> {
                            LogUtil.info(TAG_LOG + TAG_2, "Error in subscribe in onActive "
                                    + throwable.getMessage());
                        }
                );
    }
}
